<?php
namespace App\Helpers;

use App\Contracts\CreateAppToSendAgreementInterface;
use App\Contracts\CreateAppToSendInterface;
use App\Contracts\SendInterface;
use Illuminate\Support\Facades\DB;

class Application {

    private $appToSendArray;
    private $appId;
    private $bankId;

    public function __construct($appId=null, $bankId=null) {
        $this->appId = $appId;
        $this->bankId = $bankId;
    }

    public static function genMidgardHash($string, $key) :string
    {
        return base64_encode(hash_hmac("sha256", $string, $key, true));
    }

    public function deleteReturnPayPart()
    {
        DB::table('tf.banks_return_paypart')
            ->where([
                ['app_id', (int)$this->appId],
                ['bank_id', (int)$this->bankId]
            ])->delete();
    }

    public function getBanksReturnPayPartApp()
    {
        $result = DB::table('tf.banks_return_paypart')
            ->where([
                ['app_id', (int)$this->appId],
                ['bank_id', (int)$this->bankId]
            ])
            ->select('full_return', 'sum')
            ->orderBy('id', 'desc')
            ->first();

        if (empty($result)){
            return false;
        }
        return $result;
    }

    public function insertBanksReturnPayPart(array $dataArray)
    {
        DB::table('tf.banks_return_paypart')->insert(
            [
                'app_id'      => (int)$this->appId,
                'bank_id'     => (int)$this->bankId,
                'date_add'    => date('Ymd H:i:s', time()),
                'full_return' => (int)$dataArray['param_full'],
                'sum'         => (float)$dataArray['sum'],
            ]);
    }

    public function getCurrentInfoApp($appId = null, $bankId = null)
    {
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;

        $result = DB::table('tf.app_banks')
            ->where([
                ['app_id', (int)$appId],
                ['bank_id', (int)$bankId]
            ])
            ->select('ab_phase_id', 'ab_error_code', 'ab_answer', 'ab_confirm_sent')
            ->first();

        if (empty($result)){
            return false;
        }
        return $result;
    }

    public function insertGoodFullRefund($summa)
    {
        DB::table('tf.goods')->insert(
            [
              'app_id' => (int)$this->appId,
              'good_id' => config('bankConfig.good_id_full'),
              'good_group_id' => config('bankConfig.good_group_id'),
              'good_subgroup_id' => config('bankConfig.good_subgroup_id'),
              'good_mark' => "Відміна",
              'good_model' => "Повна",
              'goos_cost' => -$summa,
            ]);
    }

    public function insertGoodPartRefund($summa)
    {
        DB::table('tf.goods')->insert(
            [
                'app_id' => (int)$this->appId,
                'good_id' => config('bankConfig.good_id_part'),
                'good_group_id' => config('bankConfig.good_group_id'),
                'good_subgroup_id' => config('bankConfig.good_subgroup_id'),
                'good_mark' => "Відміна",
                'good_model' => "Часткова",
                'goos_cost' => -$summa,
            ]);
    }

    public function existAppId($appId)
    {
        return DB::table('tf.applications')->where('id','=', (int)$appId)->exists();
    }

    public function updateOrinsertBanksProducts($updateArr) : void {

        DB::table('tf.banks_products')
            ->updateOrInsert(
                ['app_id' => $this->appId, 'bank_id' => $this->bankId],
                $updateArr
            );

    }

    public function addAppToSendArray(array $array)
    {
        $this->appToSendArray = $array;
    }

    public function checkErrors($appId = null) {
        $appId = (empty($appId)) ? $this->appId : $appId;
        $result =  DB::table('tf.error_cash as ec')
            ->select('ec.id_error', 'smec.table_column')
            ->leftJoin('tf.sm_error_cash as smec', 'ec.id_error', '=', 'smec.id_error')
            ->where([
                ['id_app', (int)$appId],
                ['correction', 1]
            ])
            ->get();
        return json_decode(json_encode($result), true);
    }

    public function checkIfneedDelivery($appId = null){
        $appId = (empty($appId)) ? $this->appId : $appId;
        return DB::table('tf.delivery_courier as dc')
            ->where([
                ['dc.application_number', $appId],
                ['a.app_type', 500016]
            ])
            ->join('tf.applications as a', 'a.id', '=', 'dc.application_number')
            ->exists();
    }

    public function checkIfSendForDelivery($appId = null){
        $appId = (empty($appId)) ? $this->appId : $appId;
        $result  = DB::table('tf.delivery_courier')
                 ->select('sent')
                 ->where('application_number', $appId)
                 ->first();

        if (!$result) {
            return false;
        }

        return $result->sent == '1' ? true : false;
    }

    public function createAppToSend(CreateAppToSendInterface $bank, $app) {
        return $bank->create($app);
    }

    public function createAppToSendAgreement(CreateAppToSendAgreementInterface $bank, $leadId, $proposalId) {
        return $bank->create($leadId, $proposalId);
    }

    public function deleteErrors($appId = null){
        $appId = (empty($appId)) ? $this->appId : $appId;
        DB::table('tf.error_cash')
            ->where([
                ['id_app', (int)$appId],
                ['correction', 1]
            ])
            ->delete();
    }

    public function getAppArray() {
        return $this->appToSendArray;
    }

    public function getSms($appId = null, $bankId = null) {
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;
        $result = DB::table('tf.app_banks')
            ->where([
                ['app_id', (int)$appId],
                ['bank_id', (int)$bankId]
            ])
            ->select('ab_sms_code')
            ->first();
        return $result->ab_sms_code;
    }

    public function getGuid($appId = null) {
        $appId = (empty($appId)) ? $this->appId : $appId;

        $result = DB::table('tf.applications')
            ->select('guid_id')
            ->where('id', $appId)
            ->first();

        if (empty($result)){
            return false;
        }
        return $result->guid_id;
    }

    public function getCurrentPhaseApp($appId = null, $bankId = null){
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;
        $result = DB::table('tf.app_banks')
            ->where([
                ['app_id', (int)$appId],
                ['bank_id', (int)$bankId]
            ])
            ->select('ab_phase_id')
            ->first();

        if (empty($result)){
            return false;
        }
        return $result->ab_phase_id;
    }

    public function getIdNets($appId=null) {
        $appId = (empty($appId)) ? $this->appId : $appId;

        $result = DB::table('tf.applications as a')
            ->select('mrn.id_nets')
            ->leftJoin('tf.m_region_nets as mrn', 'mrn.id', '=', 'a.id_tt')
            ->where('a.id', $appId)
            ->first();

        if (empty($result)){
            return false;
        }
        return $result->id_nets;
    }

    public function getHistoryPhaseAppForBank($logPhaseNew, $appId=null, $bankId=null){
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;

        $result = DB::table('tf.log_app_banks')
            ->select(
                'log_phase_new'
            )
            ->where([
                ['app_id', $appId],
                ['bank_id', $bankId],
                ['log_phase_new', $logPhaseNew]
            ])
            ->first();

        if (is_null($result)){
            return false;
        }
        return true;
    }

    public function getPhaseByLead($leadId) {
        $result = DB::table('tf.app_banks')
            ->where([
                ['ab_lead_id', (string)$leadId],
            ])
            ->select('ab_phase_id')
            ->first();

        return $result->ab_phase_id;
    }

    public function getAppId($leadId, $bankId = null) {
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;
        $result = DB::table('tf.app_banks')
            ->where([
                ['ab_lead_id', (string)$leadId],
                ['bank_id', (int)$bankId]
            ])
            ->select('app_id')
            ->first();

        if (empty($result)){
            return false;
        }

        return $result->app_id;
    }

    public function getLeadId($appId = null, $bankId = null) {
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;
        $result = DB::table('tf.app_banks')
            ->where([
                ['app_id', (int)$appId],
                ['bank_id', (int)$bankId]
            ])
            ->select('ab_lead_id')
            ->first();

        if (empty($result)){
            return false;
        }

        return $result->ab_lead_id;
    }

    public function getAppType($appId = null) {
        $appId = (empty($appId)) ? $this->appId : $appId;

        $result = DB::table('tf.applications')
            ->select('app_type')
            ->where([
                ['id', $appId]
            ])
            ->first();

        if (empty($result)){
            return false;
        }

        return $result->app_type;
    }

    public function getInfoforStatus($leadId, $bankId = null) {
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;
        return DB::table('tf.app_banks')
            ->select(
                'app_id',
                'ab_phase_id',
                'ab_bank_state',
                'ab_answer',
                'ab_card_phase_id'
            )
            ->where([
                ['ab_lead_id', (string)$leadId],
                ['bank_id', (int)$bankId]
            ])
            ->first();
    }

    public function makeJson() {
        return json_encode($this->appToSendArray, JSON_PRESERVE_ZERO_FRACTION | JSON_UNESCAPED_UNICODE);
    }

    public function makeXML(){
        return "UNDER CONSTRUCTION 8->";
    }

    public function setClientDecline($appId=null, $bankId=null) {
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;

        DB::table('tf.app_banks')
            ->where([
                ['app_id', $appId],
                ['bank_id', '<>' ,$bankId],
                ['ab_error_code', 0],
                ['ab_answer', '<>', 'REF']
            ])
            ->whereNotIn('ab_phase_id', [9, 10])
            ->update([
                'ab_phase_id' => 9
            ]);
    }

    public function setAppId($appId) : void{
        $this->appId = $appId;
    }

    public function setBankId($bankId) : void{
        $this->bankId = $bankId;
    }

    public function send(SendInterface $send, $app, $method) {
        return $send->send($app, $method);
    }

    public function updateAppBanks($updateArr) : void {
        DB::table('tf.app_banks')
            ->where([
                ['bank_id', $this->bankId],
                ['app_id', $this->appId]
            ])
            ->update($updateArr);
    }

    public function updateAppForPhase($appId, $bankId, $phase){
        DB::table('tf.app_banks')
            ->where([
                ['bank_id', $bankId],
                ['app_id', $appId]
            ])
            ->update([
                'ab_phase_id'   => $phase,
                'ab_error_code' => 0,
                'ab_answer'     => 'OK'
            ]);
    }

    public function ifApprovedInOtherBank($appId=null, $bankId=null) {
        $appId = (empty($appId)) ? $this->appId : $appId;
        $bankId = (empty($bankId)) ? $this->bankId : $bankId;
        return DB::table('tf.app_banks')
            ->where([
                ['app_id', $appId],
                ['bank_id', '<>', $bankId]
            ])
            ->whereIn('ab_phase_id', [7, 8])
            ->exists();
    }
}
