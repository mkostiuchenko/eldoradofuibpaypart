<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Docums extends Controller {

    public function getDocsFolder($appId, $folder) {
        $files = array();
        $path = config('logging.logpath') . $folder . DIRECTORY_SEPARATOR . date('m') . '_' . date('Y') . DIRECTORY_SEPARATOR . date('d') . DIRECTORY_SEPARATOR;
        foreach(glob($path . "docs" . DIRECTORY_SEPARATOR . "*" . $appId . ".pdf") as $fileName){
            $files[] = $fileName;
        }
        return $files;
    }

    public function getInfoForDocuments($appId, $bankId) {
        $data = DB::table('tf.app_banks as ab')
            ->select(
                'a.id_tt',
                'a.guid_id',
                'a.app_type',
                'mrn.id_nets'
            )
            ->leftJoin('tf.applications as a', 'a.id', '=', 'ab.app_id')
            ->leftJoin('tf.m_region_nets as mrn', 'mrn.id', '=', 'a.id_tt')
            ->where([
                ['ab.app_id', $appId],
                ['ab.bank_id', $bankId]
            ])
            ->get();

        return json_decode(json_encode($data), true);
    }

    public function mergePDF($filesPdfArray, $appId, $name, $bankId, $getDocums) {
        $pdf = new \PDFMerger();

        foreach ($filesPdfArray as $item) {
            $pdf->addPDF($item, 'all');
        }

        $res = $this->getInfoForDocuments($appId, $bankId);
        $fileDocName = $name . "_" . $res[0]['guid_id'] . ".pdf";
        $path = $getDocums . $res[0]['id_nets'] . DIRECTORY_SEPARATOR . $fileDocName;

        $pdf->merge('file', $path);

        foreach ($filesPdfArray AS $file) {
            unlink($file);
        }
    }

    public function getDocsListForCash($appId, $type) {
        return DB::table('tf.scan_doc')
            ->where([
                ['app_id', $appId],
                ['type', $type]
            ])
            ->select('path_scan')
            ->get();
    }

    public function getPathScan($appId) {
        $result = DB::table('tf.scan_doc')
            ->where([
                ['app_id', $appId]
            ])
            ->select('path_scan')
            ->first();
        return $result->path_scan;
    }

    public function deleteDocs($appId, $type) {
        DB::table('tf.scan_doc')
            ->where([
                ['app_id', $appId],
                ['type', $type]
            ])
            ->delete();
    }

    public function saveDocuments($path, $name, $doc) {
        if (!file_exists(config('logging.saveDocs') . $path)) {
            mkdir(config('logging.saveDocs') . $path, 0777, true);
        }
        file_put_contents(config('logging.saveDocs') . $path . $name, $doc);
    }
}
