<?php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;
class ErrorCount {

    private $bankId;

    public function __construct($bankId) {
        $this->bankId = $bankId;
    }

    public function get($appId) {
        $res = DB::table('tf.app_banks')
            ->where([
                ['app_id', $appId],
                ['bank_id', $this->bankId]
            ])
            ->select('ab_err_count')
            ->get();

        $error = 0;

        if (isset($res['ab_err_count'])) {
            $error = $res['ab_err_count'];
        }

        return $error;
    }

    public function set($appId, $count = 1) {
        DB::table('tf.app_banks')
            ->where([
                ['app_id', $appId],
                ['bank_id', $this->bankId]
            ])
            ->increment('ab_err_count', $count);
    }
}
