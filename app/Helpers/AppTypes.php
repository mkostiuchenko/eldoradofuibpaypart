<?php
namespace App\Helpers;

class AppTypes {

    private static $appTypes = [
        'FUIB_PAYPART' => [16],
    ];

    public static function getAppTypes($bankName){
        return self::$appTypes[$bankName];
    }
}
