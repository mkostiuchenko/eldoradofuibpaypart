<?php

namespace App\Helpers;

class Logger {

    private $bank;
    private $logName;
    private $dirArray = array('tmp', 'out', 'in', 'docs');

    public function __construct($bank=null, $logName=null) {
        $this->bank = $bank;
        $this->logName = $logName;
    }

    public function setLogName($logName) {
        $this->logName = $logName;
    }

    public function loggingPath($dirArray, $folder) {
        $logPath = config('logging.logpath') . $folder . DIRECTORY_SEPARATOR . date('m') . '_' . date('Y') . DIRECTORY_SEPARATOR . date('d');

        if (!file_exists($logPath)) {
            mkdir($logPath, 0777, true);
        }
        foreach ($dirArray AS $dir) {
            if (!file_exists($logPath . DIRECTORY_SEPARATOR . $dir)) {
                mkdir($logPath . DIRECTORY_SEPARATOR . $dir, 0777);
            }
        }
        return $logPath;
    }

    public function makeLog($log) {

        $path = $this->loggingPath($this->dirArray, $this->bank);
        file_put_contents($path. DIRECTORY_SEPARATOR. $this->logName . ".log" , '[' . date('d.m.Y') . ' ' . date('H:i:s') . '] ' . $log . "\r\n", FILE_APPEND);
    }

    public function makeLogOut($appId, $log) {
        $path = $this->loggingPath($this->dirArray, $this->bank);
        file_put_contents($path. DIRECTORY_SEPARATOR . "out" . DIRECTORY_SEPARATOR . $this->logName . "_Request_" . $appId . ".log", '[' . date('d.m.Y') . ' ' . date('H:i:s') . '] ' . $log . "\r\n", FILE_APPEND);
    }

    public function makeLogIn($appId, $log) {
        $path = $this->loggingPath($this->dirArray, $this->bank);
        file_put_contents($path. DIRECTORY_SEPARATOR . "in" . DIRECTORY_SEPARATOR . $this->logName . "_Responce_" . $appId . ".log", '[' . date('d.m.Y') . ' ' . date('H:i:s') . '] ' . $log . "\r\n", FILE_APPEND);
    }

    public function makeLogError($appId, $log) {
        $path = $this->loggingPath($this->dirArray, $this->bank);
        file_put_contents($path. DIRECTORY_SEPARATOR . "error" . DIRECTORY_SEPARATOR . $this->logName . "_" . $appId . ".log", '[' . date('d.m.Y') . ' ' . date('H:i:s') . '] ' . $log . "\r\n", FILE_APPEND);
    }

    public function makeLogDoc($docName, $doc) {
        $path = $this->loggingPath($this->dirArray, $this->bank);
        file_put_contents($path . DIRECTORY_SEPARATOR . "docs" . DIRECTORY_SEPARATOR . $docName . ".pdf",  $doc , FILE_APPEND);
    }
}
