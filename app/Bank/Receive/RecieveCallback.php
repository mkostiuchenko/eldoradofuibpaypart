<?php

namespace App\Bank\Receive;

use App\Contracts\RecieveInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use App\Bank\Set\SetGaranteeLetter;

class RecieveCallback implements RecieveInterface
{
    public function recieve($request)
    {
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.receiveCallback'));
        $application = new Application(null, config('bankConfig.bankId'));

        $appId = $application->getAppId($request->cap_id);
        $application->setAppId($appId);

        $infoApp = $application->getCurrentInfoApp();

        $data = $request->all();
        $logger->makeLogIn($appId, json_encode($data, JSON_UNESCAPED_UNICODE));

        //ЗАЯВКА В ОШИБКЕ
        if (isset($infoApp->ab_error_code) && !empty($infoApp->ab_error_code) && ($infoApp->ab_error_code == 1)) {
            $logger->makeLogIn($appId, "Заявка $appId в ошибке!");

            $itog['statusCode'] = 1;
            $itog['statusText'] = 'Нельзя получить статус по заявке, которая находится в ошибке!';
            return $itog;
        }

        //ОТКАЗ КЛИЕНТА
        if (
             !in_array($infoApp->ab_phase_id, [0, 9, 10, 8]) &&
             ($infoApp->ab_answer != 'REF') &&
             (($request->state == 'CANCELED_BY_CLIENT') || ($request->state == 'CANCELED_BY_STORE'))
        ) {
            $application->updateAppBanks([
                'ab_phase_id'       => (int)10,
                'ab_error_code'     => (int)0,
                'ab_answer'         => (string)'OK',
                'ab_bank_state'     => (string)$request->state,
                'ab_answer_comment' => 'Клиент/Партнер отменил оформление кредита!',
            ]);

            $logger->makeLogIn($appId,'Параметр ' . $request->state . ' получен корректно!');

            $itog['statusCode'] = 0;
            $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            return $itog;
        }

        //ОТКАЗ БАНКА
        if (
            !in_array($infoApp->ab_phase_id, [0, 9, 10, 8]) &&
            ($infoApp->ab_answer != 'REF') &&
            (($request->state == 'REJECTED') || ($request->state == 'FAIL') || ($request->state == 'FAIL_OTP'))
        ) {
            $application->updateAppBanks([
                'ab_phase_id'       => (int)2,
                'ab_error_code'     => (int)0,
                'ab_answer'         => (string)'REF',
                'ab_bank_state'     => (string)$request->state,
                'ab_answer_comment' => 'Банк отменил оформление кредита!',
            ]);

            $logger->makeLogIn($appId,'Параметр ' . $request->state . ' получен корректно!');
            $itog['statusCode'] = 0;
            $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            return $itog;
        }

        //ОШИБКА НА СТОРОНЕ БАНКА ПО РАЗНЫМ ПРИЧИНАМ
        if (
            !in_array($infoApp->ab_phase_id, [0, 9, 10, 8]) &&
            ($infoApp->ab_answer != 'REF') &&
            (
              ($request->state == 'NO_LIMIT') ||
              ($request->state == 'OVER_LIMIT') ||
              ($request->state == 'CLIENT_NOT_FOUND') ||
              ($request->state == 'PUSH_TIMEOUT') ||
              ($request->state == 'CONFIRM_TIME_EXPIRED') ||
              ($request->state == 'FAILED_TO_CONFIRM')
            )
        ) {
            $application->updateAppBanks([
                'ab_error_code'     => (int)1,
                'ab_answer'         => (string)'ERR',
                'ab_bank_state'     => (string)$request->state,
                'ab_answer_comment' => (string)$request->state,
                'ab_error_text'     => (string)$request->state,
            ]);

            $logger->makeLogIn($appId,'Параметр ' . $request->state . ' получен корректно!');
            $itog['statusCode'] = 0;
            $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            return $itog;
        }

        //ПОЛУЧЕНИЕ ДОГОВОРА
        if ($request->state == 'WAITING_STORE_CONFIRM') {
            if ($infoApp->ab_phase_id != 1) {
                $logger->makeLogIn($appId,'Параметр ' . $request->state . ' получен на неверном шаге бизнес процесса!');
                $itog['statusCode'] = 1;
                $itog['statusText'] = 'Не верный шаг бизнес процесса!';
            } else {
                $request->guid_id = $application->getGuid();
                $garanteeLetter = (new SetGaranteeLetter())->set($request);
                $application->updateAppBanks([
                    'ab_guarantee_sent'  => 1,
                    'ab_phase_id'        => (int)7,
                    'ab_error_code'      => (int)0,
                    'ab_answer'          => (string)'OK',
                    'ab_answer_comment'  => (string)$request->state,
                    'ab_deal_number'     => (string)$garanteeLetter['contract_number'],
                ]);
                $itog['statusCode'] = 0;
                $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            }
            return $itog;
        }

        //ФИНАЛИЗАЦИЯ СДЕЛКИ
        if ($request->state == 'FUNDED') {
            if (in_array($infoApp->ab_phase_id, [0, 1, 2, 3, 4, 5, 6, 8, 9, 10]) || ($infoApp->ab_confirm_sent != 1)) {
                $itog['statusCode'] = 1;
                $itog['statusText'] = 'Не верный шаг бизнес процесса!';
            } else {
                $application->updateAppBanks([
                    'ab_phase_id'       => (int)8,
                    'ab_error_code'     => (int)0,
                    'ab_answer'         => (string)'OK',
                    'ab_bank_state'     => (string)$request->state,
                    'ab_answer_comment' => (string)$request->state,
                ]);

                $logger->makeLogIn($appId,'Параметр ' . $request->state . ' получен корректно!');
                $itog['statusCode'] = 0;
                $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            }
            return $itog;
        }

        //ОТМЕНА ВОЗВРАТА
        if (
            isset($infoApp->ab_phase_id) &&
            !empty($infoApp->ab_phase_id) &&
            ($infoApp->ab_phase_id == 8) &&
            (($request->state == 'CANCELED_BY_CLIENT') || ($request->state == 'FAILED_TO_CONFIRM'))
        ) {
            $application->deleteReturnPayPart();

            $logger->makeLogIn($appId,'Параметр ' . $request->state . ' получен корректно!');
            $itog['statusCode'] = 0;
            $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            return $itog;
        }

        //КОРРЕКТНЫЙ ВОЗВРАТ ТОВАРА
        if (
            isset($infoApp->ab_phase_id) &&
            !empty($infoApp->ab_phase_id) &&
            ($infoApp->ab_phase_id == 8) &&
            ($request->state == 'REFUND_FINISHED')
        ) {
            $infoReturnApp = $application->getBanksReturnPayPartApp();

            $itog['statusCode'] = 1;
            $itog['statusText'] = 'Нет данных по возврату!';

            if (
                isset($infoReturnApp->full_return) &&
                isset($infoReturnApp->sum) &&
                !empty($infoReturnApp->sum)
            ) {
                if ($infoReturnApp->full_return == 1) {
                    //Полный возврат
                    $application->insertGoodFullRefund($infoReturnApp->sum);
                } else {
                    //Частичный возврат
                    $application->insertGoodPartRefund($infoReturnApp->sum);
                }

                $itog['statusCode'] = 0;
                $itog['statusText'] = 'Параметр ' . $request->state . ' получен корректно!';
            }

            $logger->makeLogIn($appId,$itog['statusText']);
            return $itog;
        }

        $itog['statusCode'] = 1;
        $itog['statusText'] = 'Не верный шаг бизнес процесса!';
        $logger->makeLogIn($appId,$itog['statusText']);

        return $itog;
    }
}
