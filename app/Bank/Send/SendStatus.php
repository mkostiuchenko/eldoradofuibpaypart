<?php
namespace App\Bank\Send;

use App\Contracts\SendRequestInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use Illuminate\Support\Facades\App;
use App\Http\Models\Errors;

class SendStatus implements SendRequestInterface
{
    public function send($request) {
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.setStatus'));
        $application = new Application($request->app_id, config('bankConfig.bankId'));

        $result = $application->send(App::make(Send::class), ['messageId' => $request->ab_lead_id], config('bankConfig.getLoanStatus'));
        $logger->makeLogIn($request->app_id, json_encode($result, JSON_UNESCAPED_UNICODE));

        if ($result['state'] != "OK") {
            Errors::appStatusUpdateInError($request->app_id, config('bankConfig.bankId'), json_encode($result, JSON_UNESCAPED_UNICODE));
            return;
        }

        if (($result['state'] == "OK") && (trim($result['result']['statusCode']) != 'PURCHASE_IS_OK')) {
            Errors::appStatusUpdateInError($request->orderId, config('bankConfig.bankId'), $result['result']['statusText']);
            return;
        }

        if (
            isset($result['result']['statusCode']) &&
            !empty($result['result']['statusCode']) &&
            (trim($result['result']['statusCode']) == 'PURCHASE_IS_OK')
        ) {
            $application->updateAppBanks([
                'ab_phase_id'       => 5,
                'ab_answer'         => 'OK',
            ]);
        }
    }
}
