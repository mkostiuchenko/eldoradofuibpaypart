<?php
namespace App\Bank\Send;

use App\Contracts\SendInterface;
use App\Helpers\Application;
use Cassandra\Exception\TruncateException;

class Send implements SendInterface
{
    public function getTokenFuib()
    {
        $ch = curl_init(config('bankConfig.token_url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));

        $postFields = http_build_query(array(
            'client_secret' => "",
            'grant_type'    => config('bankConfig.token_grantType'),
            'client_id'     => config('bankConfig.token_clientId'),
            "username"      => config('bankConfig.token_userName'),
            "password"      => config('bankConfig.token_password'),
            "code"          => "",
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

        $response = curl_exec($ch);
        return json_decode($response, true);
    }

    public function send($app, $method)
    {
        $ret_res  = array();
        $token = $this->getTokenFuib();

        switch ($method) {
            case config('bankConfig.newLoan'):
            case config('bankConfig.sendRefund'):
                $flag = 'post';
                break;
            case config('bankConfig.confirmloan'):
                $flag = 'patch';
                break;
            default:
                $flag = 'post';
        }

        $header = array(
            'Content-Type: application/json',
            'X-Flow-Id: ' . $app['appId'],
            'Authorization: Bearer ' . $token['access_token'],
        );

        $options = array();

        if ($flag == 'post') {
            //Отправка POST запроса
            $options = array(
                CURLOPT_URL => config('bankConfig.bank_url') . $method,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $app['json'],
                CURLOPT_TIMEOUT => 300,
                CURLOPT_CONNECTTIMEOUT => 300,
            );
        } elseif($flag == 'patch') {
            //Отправка PATH запроса
            $options = array(
                CURLOPT_URL => config('bankConfig.bank_url') . $method . $app['lead'],
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS => $app['json'],
                CURLOPT_TIMEOUT => 300,
                CURLOPT_CONNECTTIMEOUT => 300,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            );
        }

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!$response && (!in_array($httpCode, [200, 201]))) {
            $ret_res['state']      = 'ERR';
            $ret_res['info']       = curl_getinfo($ch);
            $ret_res['error_text'] = curl_error($ch);
            $ret_res['error_no']   = curl_errno($ch);
            $ret_res['result']     = curl_error($ch);
            $ret_res['httpcode']   = $httpCode;

            return $ret_res;
        }

        $ret_res['state']           = 'OK';
        $ret_res['info']            = curl_getinfo($ch);
        $ret_res['result_decode']   = json_decode($response, true);
        $ret_res['result']          = $response;
        $ret_res['httpcode']        = $httpCode;

        curl_close($ch);
        return $ret_res;
    }
}
