<?php

namespace App\Bank\Send;

use App\Contracts\SendRequestInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use App\Http\Models\Errors;
use Illuminate\Support\Facades\App;

class SendApprove implements SendRequestInterface
{
    public function send($request)
    {
        $application = new Application($request->app_id, config('bankConfig.bankId'));

        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.sendApprove'));

        $arr['method']         = "UPDATE" ;
        $arr['goods_shipped']  = true;
        $arr['flow'] ['type']  = config('bankConfig.flowtype');
        $application->addAppToSendArray($arr);

        $appToSend = [
            'json' => $application->makeJson(),
            'appId' => $request->app_id,
            'lead'  => $request->ab_lead_id,
        ];

        $logger->makeLogOut($request->app_id, json_encode($appToSend, JSON_UNESCAPED_UNICODE));
        $result = $application->send(App::make(Send::class), $appToSend, config('bankConfig.confirmloan'));
        $logger->makeLogIn($request->app_id, json_encode($result['result'], JSON_UNESCAPED_UNICODE));

        if ($result['state'] != "OK") {
            Errors::appStatusUpdateInError($request->app_id, config('bankConfig.bankId'), $result['result']);
            return;
        }

        if (($result['state'] == "OK") && (!in_array($result['httpcode'], [200, 201]))) {
            Errors::appStatusUpdateInError($request->app_id, config('bankConfig.bankId'), $result['result']);
            return;
        }

        $application->updateAppBanks([
            /*'ab_phase_id'       => 8,
            'ab_answer'         => 'OK',*/
            'ab_confirm_sent' => 1
        ]);

    }
}
