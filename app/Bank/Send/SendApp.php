<?php
namespace App\Bank\Send;

use App\Bank\Create\CreateAppToSend;
use App\Contracts\SendRequestInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use Illuminate\Support\Facades\App;
use App\Http\Models\Errors;

class SendApp implements SendRequestInterface
{
    public function send($request)
    {
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.sendApp'));
        $application = new Application($request->orderId, config('bankConfig.bankId'));

        $appToSendArr = $application->createAppToSend(App::make(CreateAppToSend::class), $request);

        $application->addAppToSendArray($appToSendArr);
        $appToSend = [
                         'json' => $application->makeJson(),
                         'appId' => $request->orderId,
                     ];

        if (empty($appToSend)) {
            return;
        }

        $logger->makeLogOut($request->orderId, json_encode($appToSend, JSON_UNESCAPED_UNICODE));
        $result = $application->send(App::make(Send::class), $appToSend, config('bankConfig.newLoan'));
        $logger->makeLogIn($request->orderId, $result['result']);

        if ($result['state'] != "OK") {
            Errors::appStatusUpdateInError($request->orderId, config('bankConfig.bankId'), json_encode($result['result'], JSON_UNESCAPED_UNICODE));
            return;
        }

        if (($result['state'] == "OK") && (!in_array($result['httpcode'], [200, 201]))) {
            Errors::appStatusUpdateInError($request->orderId, config('bankConfig.bankId'), $result['result']);
            return;
        }

        if (
            isset($result['result_decode']['id']) &&
            !empty($result['result_decode']['id'])
        ) {
            $application->updateAppBanks([
                'ab_phase_id'       => (int)1,
                'ab_error_code'     => (int)0,
                'ab_answer'         => (string)'OK',
                'ab_date_start'     => date('Ymd H:i:s', time()),
                'ab_lead_id'        => (string)$result['result_decode']['id'],
                'ab_cancel_sent'    => (int)0,
                'ab_bank_state'     => (string)'NEW',
                'ab_confirm_sent'   => (int)0,
            ]);
        }
    }
}
