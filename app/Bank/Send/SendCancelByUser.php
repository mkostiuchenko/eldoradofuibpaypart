<?php

namespace App\Bank\Send;

use App\Contracts\SendRequestInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use App\Http\Models\Errors;
use Illuminate\Support\Facades\App;

class SendCancelByUser implements SendRequestInterface
{
    public function send($request)
    {
        $application = new Application($request->app_id, config('bankConfig.bankId'));
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.sendCancelByUser'));

        $arr['messageId']    = $request->ab_lead_id;
        $arr['cancelId']     = "cancelPart-" . $request->app_id;
        $arr['reasonCancel'] = "відмова клієнта";

        $application->addAppToSendArray($arr);
        $appToSend = $application->makeJson();

        $logger->makeLogOut($request->app_id, $appToSend);
        $result = $application->send(App::make(Send::class), $appToSend, config('bankConfig.sendCancelOrder'));
        $logger->makeLogIn($request->app_id, json_encode($result['result'], JSON_UNESCAPED_UNICODE));

        if ($result['state'] != "OK") {
            Errors::appStatusUpdateInError($request->app_id, config('bankConfig.bankId'), json_encode($result['result'], JSON_UNESCAPED_UNICODE));
            return;
        }

        if (
            ($result['state'] == "OK") &&
            (trim($result['result']['statusCode']) != 'CANCEL_IS_OK') &&
            (trim($result['result']['statusCode']) != 'CANCEL_AFTER_OK')
        ) {
            Errors::appStatusUpdateInError($request->orderId, config('bankConfig.bankId'), $result['result']['statusText']);
            return;
        }

        if (
            isset($result['result']['statusCode']) &&
            !empty($result['result']['statusCode']) &&
            (trim($result['result']['statusCode']) == 'CANCEL_IS_OK')
        ) {
            $application->updateAppBanks([
                'ab_cancel_sent'  => 1,
                'ab_bank_state'   => 'CANCEL_IS_OK',
            ]);
        }

    }


}
