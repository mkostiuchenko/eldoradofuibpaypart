<?php
namespace App\Bank\Send;

use App\Contracts\SendRequestInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use Illuminate\Support\Facades\App;
use App\Http\Models\Errors;
use PDF;

class SendGaranteeLetter implements SendRequestInterface
{
    public function send($request) {
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.getGaranteeLetter'));
        $application = new Application($request->app_id, config('bankConfig.bankId'));

        $result = $application->send(App::make(Send::class), ['messageId' => $request->ab_lead_id], config('bankConfig.getWarranty'));
        $logger->makeLogIn($request->app_id, json_encode($result, JSON_UNESCAPED_UNICODE));

        if ($result['state'] != "OK") {
            Errors::appStatusUpdateInError($request->app_id, config('bankConfig.bankId'), json_encode($result['result'], JSON_UNESCAPED_UNICODE));
            return;
        }

        if (($result['state'] == "OK") && (trim($result['result']['statusCode']) != 'GUARANTEE_MAIL_OK')) {
            Errors::appStatusUpdateInError($request->orderId, config('bankConfig.bankId'), $result['result']['statusText']);
            return;
        }

        if (
            isset($result['result']['statusCode'], $result['result']['base64Pdf']) &&
            !empty($result['result']['statusCode']) &&
            !empty($result['result']['base64Pdf']) &&
            (trim($result['result']['statusCode']) == 'GUARANTEE_MAIL_OK')
        ) {
            $file = base64_decode($result['result']['base64Pdf']);
            file_put_contents(config('logging.saveDocs') . "alfabank_guarantee_" . $request->guid_id . ".pdf", $file);

            $logger->makeLogIn($request->app_id, "Сделка финализирована!");

            $application->updateAppBanks([
                'ab_phase_id'       => 8,
                'ab_answer'         => 'OK',
            ]);
        }
    }
}
