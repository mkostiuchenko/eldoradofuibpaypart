<?php

namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use Illuminate\Support\Facades\App;
use App\Bank\Send\Send;
use App\Http\Models\Errors;

class SetRefundData implements SetInterface
{
    public function set($request) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '2100');
        $data = $request->all();

        $application = new Application(null, config('bankConfig.bankId'));
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.setRefund'));

        if (
            !$request->hasHeader('token') ||
            ($request->header('token') != config('bankConfig.tokenInterface'))
        ) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Authoarization filed!';
            return $itog;
        }

        if (!isset($data['appId']) || empty($data['appId'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за заявку appId';
            return $itog;
        }

        if (!isset($data['full_refund'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за признак возврата full_refund';
            return $itog;
        }

        if (!isset($data['sum']) || empty($data['sum'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за сумму возврата sum';
            return $itog;
        }

        if (!isset($data['point_of_sale_code']) || empty($data['point_of_sale_code'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за точку выдачи кредита point_of_sale_code';
            return $itog;
        }

        if (!isset($data['agreement_number']) || empty($data['agreement_number'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за номер договораа agreement_number';
            return $itog;
        }

        if (!$application->existAppId($data['appId'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет заявки в системе ТФ';
            return $itog;
        }

        $application->setAppId($data['appId']);
        $leadId = $application->getLeadId();

        if (empty($leadId)) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Нет идентификатора заявки на стороне банка';
            return $result;
        }

        $appToSendArr['id']                 = $leadId;
        $appToSendArr['point_of_sale_code'] = $data['point_of_sale_code'];
        $appToSendArr['agreement_number']   = $data['agreement_number'];
        $appToSendArr['amount']             = round($data['sum'], 2);
        $appToSendArr['refund']             = true;
        $appToSendArr['store_user_login']   = config('bankConfig.store_user_login');
        $appToSendArr['flow'] ['type']      = config('bankConfig.flowtype');

        $application->addAppToSendArray($appToSendArr);

        $appToSend = [
            'json' => $application->makeJson(),
            'appId' => $data['appId'],
        ];

        $logger->makeLogOut($data['appId'], $appToSend['json']);
        $result = $application->send(App::make(Send::class), $appToSend, config('bankConfig.sendRefund'));
        $logger->makeLogIn($data['appId'], $result['result']);

        if ($result['state'] != "OK") {
            $itog['res_number'] = 1;
            $itog['res_txt']    = $result['result'];

            Errors::appStatusUpdateInError($data['appId'], config('bankConfig.bankId'), (string)$result['res_txt']);
            return $itog;
        }

        if (($result['state'] == "OK")  && (!in_array($result['httpcode'], [200, 201]))) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = $result['result'];

            Errors::appStatusUpdateInError($data['appId'], config('bankConfig.bankId'), (string)$itog['res_txt']);
            return $itog;
        }

        if (
            isset($result['result_decode']['id']) &&
            !empty($result['result_decode']['id'])
        ) {
            $itog['res_number']                       = 0;
            $itog['res_txt']                          = "OK";
            //При успешном ответе status = "OK"
            $itog['status']                           = "OK";
            //Сумма товара
            $dataArray['sum'] = $data['sum'];

            if ($data['full_refund']) {
                //Полный возврат
                $dataArray['param_full'] = 1;
            } else {
                //Частичный возврат
                $dataArray['param_full'] = 0;
            }

            $application->insertBanksReturnPayPart($dataArray);

            return $itog;
        }

        $itog['res_number'] = 1;
        $itog['res_txt']    = "Не известная ошибка банка";
        return $itog;
    }
}
