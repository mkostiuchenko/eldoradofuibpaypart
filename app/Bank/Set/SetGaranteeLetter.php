<?php
namespace App\Bank\Set;

use App\Contracts\SetInterface;
use PDF;

class SetGaranteeLetter implements SetInterface
{
    public function set($request) {
        $answer = [];
        $result = $request->all();

        if (isset($result['garantee_letter']) && !empty($result['garantee_letter']) && is_array($result['garantee_letter'])) {

            $answer['id_garantee_letter'] = ""; //Идентификатор Листа-гарантии
            if (isset($result['garantee_letter']['id'])
                && !empty($result['garantee_letter']['id'])) {
                $answer['id_garantee_letter'] = $result['garantee_letter']['id'];
            }

            $answer['from_organization'] = ""; //Название банка
            if (isset($result['garantee_letter']['content']['bank']['name'])
                && !empty($result['garantee_letter']['content']['bank']['name'])) {
                $answer['from_organization'] = $result['garantee_letter']['content']['bank']['name'];
            }

            $answer['organization_id'] = ""; //ЕГРПОУ банка
            if (isset($result['garantee_letter']['content']['bank']['requisites']['nsrueo'])
                && !empty($result['garantee_letter']['content']['bank']['requisites']['nsrueo'])) {
                $answer['organization_id'] = $result['garantee_letter']['content']['bank']['requisites']['nsrueo'];
            }

            $answer['answer_datetime'] = ""; //Дата-время  ответа Формат: yyyy-MM-dd HH:MI:SS
            if (isset($result['garantee_letter']['content']['date'])
                && !empty($result['garantee_letter']['content']['date'])) {
                $answer['answer_datetime'] = $result['garantee_letter']['content']['date'];
            }

            $answer['contract_number'] = ""; //Номер договора о сотрудничестве с клиентом
            if (isset($result['garantee_letter']['content']['customer_agreement']['number'])
                && !empty($result['garantee_letter']['content']['customer_agreement']['number'])) {
                $answer['contract_number'] = $result['garantee_letter']['content']['customer_agreement']['number'];
            }

            $answer['contract_date'] = ""; //Дата договора о сотрудничестве с клиентом Формат: yyyy-MM-dd
            if (isset($result['garantee_letter']['content']['customer_agreement']['date'])
                && !empty($result['result']['header']['contract_date'])) {
                $answer['contract_date'] = $result['result']['header']['contract_date'];
            }

            $answer['request_id'] = ""; //Номер заявки
            if (isset($result['garantee_letter']['content']['cap_id'])
                && !empty($result['garantee_letter']['content']['cap_id'])) {
                $answer['request_id'] = $result['garantee_letter']['content']['cap_id'];
            }

            $answer['agreement'] = ""; //Информация о договоре между Торговой сетью и Банком - Номер договора рассрочки
            if (isset($result['garantee_letter']['content']['retailer_agreement']['number'])
                && !empty($result['garantee_letter']['content']['retailer_agreement']['number'])) {
                $answer['agreement'] = $result['garantee_letter']['content']['retailer_agreement']['number'];
            }

            $answer['agreement_date'] = ""; //Информация о договоре между Торговой сетью и Банком Дата : yyyy-MM-dd
            if (isset($result['garantee_letter']['content']['retailer_agreement']['date'])
                && !empty($result['garantee_letter']['content']['retailer_agreement']['date'])) {
                $answer['agreement_date'] = $result['garantee_letter']['content']['retailer_agreement']['date'];
            }

            $answer['first_name'] = ""; //Имя клиента
            if (isset($result['garantee_letter']['content']['customer']['first_name'])
                && !empty($result['garantee_letter']['content']['customer']['first_name'])) {
                $answer['first_name'] = $result['garantee_letter']['content']['customer']['first_name'];
            }

            $answer['last_name'] = ""; //Фамилия  клиента
            if (isset($result['garantee_letter']['content']['customer']['last_name'])
                && !empty($result['garantee_letter']['content']['customer']['last_name'])) {
                $answer['last_name'] = $result['garantee_letter']['content']['customer']['last_name'];
            }

            $answer['middle_name'] = ""; //Отчество  клиента
            if (isset($result['garantee_letter']['content']['customer']['middle_name'])
                && !empty($result['garantee_letter']['content']['customer']['middle_name'])) {
                $answer['middle_name'] = $result['garantee_letter']['content']['customer']['middle_name'];
            }

            $answer['inn'] = ""; //ИНН  клиента
            if (isset($result['garantee_letter']['content']['customer']['tax_id'])
                && !empty($result['garantee_letter']['content']['customer']['tax_id'])) {
                $answer['inn'] = $result['garantee_letter']['content']['customer']['tax_id'];
            }

            //Сумма кредита с вычтенным первоначальным взносом (разовая комиссия и страховка в этой сумме не учитываются)
            $answer['invoice_amount'] = "";
            if (isset($result['garantee_letter']['content']['net_amount'])
                && !empty($result['garantee_letter']['content']['net_amount'])) {
                $answer['invoice_amount'] = $result['garantee_letter']['content']['net_amount'];
            }

            $answer['number'] = ""; //Номер паспорта
            if (isset($result['garantee_letter']['content']['customer']['identification']['number'])
                && !empty($result['garantee_letter']['content']['customer']['identification']['number'])) {
                $answer['number'] = $result['garantee_letter']['content']['customer']['identification']['number'];
            }

            $answer['series'] = ""; //Серия паспорта
            if (isset($result['garantee_letter']['content']['customer']['identification']['series'])
                && !empty($result['garantee_letter']['content']['customer']['identification']['series'])) {
                $answer['series'] = $result['garantee_letter']['content']['customer']['identification']['series'];
            }

            $answer['date_of_issue'] = ""; //Дата выдачи паспорта Формат: yyyy-MM-dd
            if (isset($result['garantee_letter']['content']['customer']['identification']['date'])
                && !empty($result['garantee_letter']['content']['customer']['identification']['date'])) {
                $answer['date_of_issue'] = $result['garantee_letter']['content']['customer']['identification']['date'];
            }

            $answer['issued'] = ""; //Орган выдавший паспорт
            if (isset($result['garantee_letter']['content']['customer']['identification']['issuer'])
                && !empty($result['garantee_letter']['content']['customer']['identification']['issuer'])) {
                $answer['issued'] = $result['garantee_letter']['content']['customer']['identification']['issuer'];
            }

            $answer['store_address'] = ""; //Информация о магазине, где состоялась продажа в кредит Адрес магазина
            if (isset($result['garantee_letter']['content']['store']['address'])
                && !empty($result['garantee_letter']['content']['store']['address'])) {
                $answer['store_address'] = $result['garantee_letter']['content']['store']['address'];
            }

            $answer['store_code'] = ""; //Информация о магазине, где состоялась продажа в кредит Код точки продаж
            if (isset($result['garantee_letter']['content']['store']['code'])
                && !empty($result['garantee_letter']['content']['store']['code'])) {
                $answer['store_code'] = $result['garantee_letter']['content']['store']['code'];
            }

            $answer['dest_name'] = ""; //Название партнера
            if (isset($result['garantee_letter']['content']['retailer']['name'])
                && !empty($result['garantee_letter']['content']['retailer']['name'])) {
                $answer['dest_name'] = $result['garantee_letter']['content']['retailer']['name'];
            }

            $answer['dest_id'] = ""; //ЕГРПОУ партнера
            if (isset($result['garantee_letter']['content']['retailer']['nsrueo'])
                && !empty($result['garantee_letter']['content']['retailer']['nsrueo'])) {
                $answer['dest_id'] = $result['garantee_letter']['content']['retailer']['nsrueo'];
            }

            $answer['dest_bank_name'] = ""; //Название банка где у партнера открыт счет
            if (isset($result['garantee_letter']['content']['retailer']['requisites']['bank'])
                && !empty($result['garantee_letter']['content']['retailer']['requisites']['bank'])) {
                $answer['dest_bank_name'] = $result['garantee_letter']['content']['retailer']['requisites']['bank'];
            }

            $answer['dest_bank_mfo'] = ""; //МФО банка где у партнера открыт счет
            if (isset($result['garantee_letter']['content']['retailer']['requisites']['mfo'])
                && !empty($result['garantee_letter']['content']['retailer']['requisites']['mfo'])) {
                $answer['dest_bank_mfo'] = $result['garantee_letter']['content']['retailer']['requisites']['mfo'];
            }

            $answer['dest_acc_number'] = ""; //Номер счета партнера
            if (isset($result['garantee_letter']['content']['retailer']['requisites']['account_number'])
                && !empty($result['garantee_letter']['content']['retailer']['requisites']['account_number'])) {
                $answer['dest_acc_number'] = $result['garantee_letter']['content']['retailer']['requisites']['account_number'];
            }

            $answer['invoice_number'] = ""; //Номер чека/счета фактуры
            if (isset($result['garantee_letter']['content']['invoices'][0]['invoice_number'])
                && !empty($result['garantee_letter']['content']['invoices'][0]['invoice_number'])) {
                $answer['invoice_number'] = $result['garantee_letter']['content']['invoices'][0]['invoice_number'];
            }

            $answer['invoice_date'] = ""; //Дата чека/счета фактуры. Формат: yyyy-MM-dd
            if (isset($result['garantee_letter']['content']['invoices'][0]['date'])
                && !empty($result['garantee_letter']['content']['invoices'][0]['date'])) {
                $answer['invoice_date'] = $result['garantee_letter']['content']['invoices'][0]['date'];
            }

            $answer['sign'] = ""; //Электронная цифровая подпись банка. Строка в кодировке Base64
            if (isset($result['garantee_letter']['sign'])
                && !empty($result['garantee_letter']['sign'])) {
                $answer['sign'] = base64_decode($result['garantee_letter']['sign']);
            }

            $pdf = PDF::loadView('pdf.invoice', compact('answer'));
            $pdf->save(config('logging.saveDocs'). "fuib_guarantee_" . $request->guid_id . ".pdf");
        }

        return $answer;
    }
}
