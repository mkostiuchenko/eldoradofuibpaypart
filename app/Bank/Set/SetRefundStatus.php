<?php

namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use App\Bank\Send\Send;
use App\Http\Models\Errors;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class SetRefundStatus implements SetInterface
{
    public function set($request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '2100');
        $data = $request->all();

        $application = new Application(null, config('bankConfig.bankId'));
        $logger = new Logger(config('bankConfig.bankName'), config('logFileName.setRefundStatus'));

        if (!$request->hasHeader('token')) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Authoarization filed!';
            return $itog;
        }

        if ($request->header('token') != config('bankConfig.tokenInterface')) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Не корректный token';
            return $itog;
        }

        if (!isset($data['reversalSum']) || empty($data['reversalSum'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за сумму возврата';
            return $itog;
        }

        if (!isset($data['appId']) || empty($data['appId'])) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет параметра, который отвечает за заявку appId';
            return $itog;
        }

        if(!$application->existAppId($data['appId'])){
            $itog['res_number'] = 1;
            $itog['res_txt']    = 'Нет заявки в системе ТФ';
            return $itog;
        }

        $application->setAppId($data['appId']);
        /*$leadId = $application->getLeadId();

        if (empty($leadId)) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Нет идентификатора заявки на стороне банка';
            return $result;
        }*/

        $result = $application->send(App::make(Send::class), ['orderId' => $data['appId'], 'reversalSum' => $data['reversalSum']], config('bankConfig.sendRefundStatus'));
        $logger->makeLogIn($data['appId'], json_encode($result['result']));

        if ($result['state'] != "OK") {
            $itog['res_number'] = 1;
            $itog['res_txt']    = json_encode($result['result']);

            Errors::appStatusUpdateInError($data['appId'], config('bankConfig.bankId'), (string)$itog['res_txt']);
            return $itog;
        }

        if (($result['state'] == "OK") && (trim($result['result']['statusCode']) != 'CHECK_REVERSAL_IS_OK')) {
            $itog['res_number'] = 1;
            $itog['res_txt']    = $result['result']['statusText'];

            Errors::appStatusUpdateInError($data['appId'], config('bankConfig.bankId'), (string)$itog['res_txt']);
            return $itog;
        }

        if (
            isset($result['result']['statusCode']) &&
            !empty($result['result']['statusCode']) &&
            (trim($result['result']['statusCode']) == 'CHECK_REVERSAL_IS_OK')
        ) {
            $allSumCredit = $this->getSumGoods($data['appId']);

            $itog['res_number']                       = 0;
            $itog['res_txt']                          = "OK";
            //Ознака, що гроші можуть бути повернуті на картку клієнта при поверненні = true/false
            $itog['bank_can_return_money_to_card']    = true;
            //Ознака, що заявка повністю виплачена клієнтом = true/false
            $itog['fully_paid'] = true; //Хотим полный возврат
            if ($data['reversalSum'] < $allSumCredit) {
                $itog['fully_paid'] = false; //Хотим частичный возврат
            }

            return $itog;
        }

        $itog['res_number'] = 1;
        $itog['res_txt']    = "Не известная ошибка банка";

        return $itog;
    }

    private function getSumGoods($appId)
    {
        $allSum = 0;

        $result = DB::table('tf.goods')
            ->select('goos_cost as price')
            ->where('app_id', $appId)
            ->get();

        foreach ($result as $item) {
            $allSum += $item->price;
        }

        return $allSum;
    }
}
