<?php
namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use Illuminate\Support\Facades\DB;

class SetPriliminaryApproved implements SetInterface {

    public function set($request) {
        $logger = new Logger(config('banksName.cardservice_pos'), config('logFileName.setPriliminaryApproved'));

        $application = new Application(null, config('banksId.cardservice'));
        $appId = $application->getAppId($request->app_id);
        $application->setAppId($appId);

        if (empty($appId)) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Заявка не найдена';
            return $result;
        }

        $logger->makeLogIn($appId, json_encode($request->all()));

        $phaseId = $application->getCurrentPhaseApp();

        if ($phaseId != 1) {
            $result['res_number'] = 1;
            $result['res_txt'] = 'Не верная стадия бизнес процесса';
            return $result;
        }

        $application->updateAppBanks([
            'ab_phase_id' => 3,
            'ab_answer' => 'OK',
            'ab_bank_state' => 'PreliminaryApproved'
        ]);

        if ( isset($request->termCredit, $request->creditAmmount, $request->monthlyPayment, $request->firstPay)
            && !empty($request->termCredit)
            && !empty($request->creditAmmount)
            && !empty($request->monthlyPayment) ) {
            DB::table('tf.banks_products')
                ->updateOrInsert(
                    ['bank_id' => config('banksId.cardservice'), 'app_id' => $appId],
                    [ 'Term' => $request->termCredit,                     //Срок кредита в месяцах
                      'ClearAmount' => ($request->creditAmmount)/100,     //Сумма кредита
                      'MonthlyPayment' => ($request->monthlyPayment)/100, //Сумма ежемесячного платежа
                      'DownpaymentAmount' => ($request->firstPay)/100,    //Сумма первого взноса
                    ]
                );
        }

        $result['res_number'] = 0;
        $result['res_txt'] = 'OK';
        return $result;
    }
}
