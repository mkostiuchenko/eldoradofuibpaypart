<?php

namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;

class SetSignature implements SetInterface
{
    public function set($request) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '2100');

       $data = $request->all();

       if (is_array($data) && (count($data) == 0))  {
           $result['res_number'] = 1;
           $result['res_txt']    = 'Не корректные данные!';
           return $result;
       }

        return Application::genMidgardHash(json_encode($request->all()), config('bankConfig.bank_key_preprod'));
    }
}
