<?php
namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;
use App\Helpers\Logger;

class SetCancel implements SetInterface {

    public function set($request) {
        $logger = new Logger(config('banksName.cardservice_pos'), config('logFileName.setCancel'));

        $application   = new Application(null, config('banksId.cardservice'));
        $appId = $application->getAppId($request->app_id);
        $application->setAppId($appId);

        if (empty($appId)) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Заявка не найдена';
            return $result;
        }

        if ($application->getCurrentPhaseApp() == 8) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Не верная стадия бизнес процеса';
            return $result;
        }

        $logger->makeLogIn($appId, "Установлен отказ банка");

        $description = null;
        if ( isset($request->canselDescription)
            && !empty($request->canselDescription) ) {
            $description = (string)$request->canselDescription;
        }

        $application->updateAppBanks([
            'ab_phase_id'   => 2,
            'ab_answer'     => 'REF',
            'ab_answer_comment' => $description,
            'ab_error_text' => $description,
            'ab_bank_state' => 'Rejected'
        ]);

        $result['res_number'] = 0;
        $result['res_txt']    = 'OK';
        return $result;
    }
}
