<?php
namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;
use App\Helpers\Logger;

class SetCancelByClient implements SetInterface {

    public function set($request) {
        $logger = new Logger(config('banksName.cardservice_pos'), config('logFileName.setCancelByClient'));

        $application = new Application(null, config('banksId.cardservice'));
        $appId = $application->getAppId($request->app_id);
        $application->setAppId($appId);

        if (empty($appId)) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Заявка не найдена';
            return $result;
        }

        $logger->makeLogIn($appId, "Получен отказ по заявке");

        $application->updateAppBanks([
            'ab_phase_id'   => 10,
            'ab_answer'     => 'OK',
            'ab_bank_state' => 'Cancelled By Client',
        ]);

        $result['res_number'] = 0;
        $result['res_txt']    = 'OK';
        return $result;
    }
}
