<?php

namespace App\Bank\Set;

use App\Contracts\SetInterface;
use App\Helpers\Application;
use App\Helpers\Logger;
use Illuminate\Support\Facades\DB;

class SetApproved implements SetInterface {

    public function set($request) {
        $logger = new Logger(config('banksName.cardservice_pos'), config('logFileName.setApproved'));

        $application = new Application(null, config('banksId.cardservice'));
        $appId = $application->getAppId($request->app_id);
        $application->setAppId($appId);

        if (empty($appId)) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Заявка не найдена';
            return $result;
        }

        if ($application->getCurrentPhaseApp() != 7) {
            $result['res_number'] = 1;
            $result['res_txt']    = 'Не верная стадия бизнес процеса';
            return $result;
        }

        $logger->makeLogIn($appId, "Получен одобрение по кредиту от фин. компании");

        $application->updateAppBanks([
            'ab_phase_id'   => 8,
            'ab_answer'     => 'OK',
            'ab_bank_state' => 'Approved'
        ]);

        DB::table('tf.applications')
            ->where('id', $appId)
            ->update(['app_state' => 5]);

        $result['res_number'] = 0;
        $result['res_txt']    = 'OK';
        return $result;
    }
}
