<?php

namespace App\Bank\Create;

use App\Contracts\CreateAppToSendInterface;
use Illuminate\Support\Facades\DB;

class CreateAppToSend implements CreateAppToSendInterface
{
    public function create($app)
    {
        $appToSend = [];

        $appToSend['store_order_id']      = $app->orderId;

        $infoInvoice = $this->getInfoOrderInvoice($app->app_acc_num);
        $appToSend['store_user_login']    = (isset($infoInvoice['user']) && !empty($infoInvoice['user'])) ? (string)$infoInvoice['user'] : ''; //config('bankConfig.store_user_login');

        $appToSend['point_of_sale_code']  = (isset($app->name_reg_short) && !empty($app->name_reg_short)) ? $app->name_reg_short : '';
        $appToSend['channel_type']        = config('bankConfig.channel_type');
        $appToSend['flow'] ['type']       = config('bankConfig.flowtype');

        $appToSend['customer']['first_name']  = (
                                                  isset($app->app_client_name) &&
                                                  !empty($app->app_client_name)
                                                ) ? $app->app_client_name : '';

        $appToSend['customer']['last_name']   = (
                                                  isset($app->app_client_surname) &&
                                                  !empty($app->app_client_surname)
                                                ) ? $app->app_client_surname : '';

        $appToSend['customer']['middle_name'] = (
                                                  isset($app->app_client_patronymic) &&
                                                  !empty($app->app_client_patronymic)
                                                ) ? $app->app_client_patronymic : '';

        $appToSend['customer']['phone']       = (
                                                    isset($app->mPhone) &&
                                                    !empty($app->mPhone)
                                                ) ? '+38' . $app->mPhone : '';

        //Секция данных по товарам
        $goods = $this->getGoods($app->orderId);

        //Рассчитаем сумму кредита по всем товарам
        $sumAllCredits = 0;
        if (is_array($goods) && (count($goods) != 0)){
            for ($i = 0; $i < count($goods); $i++) {
                $sumAllCredits = $sumAllCredits + $goods[$i]['amount'];
            }
        }

        $appToSend['invoices'][0]['invoice_number'] = (
                                             isset($app->app_acc_num) &&
                                             !empty($app->app_acc_num)
                                         ) ? $app->app_acc_num : '';

        $appToSend['invoices'][0]['date'] = (
                                                       isset($app->app_acc_date) &&
                                                       !empty($app->app_acc_date)
                                                   ) ? $app->app_acc_date : '';

        $appToSend['invoices'][0]['goods'] = $goods;
        $appToSend['invoices'][0]['total_amount'] = $sumAllCredits; //Сумма заявки  в копейках

        $appToSend['credit_request']['term'] = (int)$app->orderTerm; //Срок кредита в месяцах

        $app->down_payment =  (float)number_format($app->down_payment, 2, '.', ''); //ПВ
        $appToSend['credit_request']['amount'] = ($sumAllCredits - $app->down_payment); //Сумма заявки

        return $appToSend;
    }

    private function getInfoOrderInvoice($numInvoice): array
    {
        $invoice = DB::table('tf.app_invoices')
            ->select('inv_tt_code as ttCode', 'inv_saller_code as sellerCode')
            ->where('inv_number', $numInvoice)
            ->first();

        $arrData = [];
        if (isset($invoice) && !empty($invoice)) {
            $tt = (isset($invoice->ttCode) && !empty($invoice->ttCode)) ? (string)$invoice->ttCode : '';
            $seller = (isset($invoice->sellerCode) && !empty($invoice->sellerCode)) ? (string)$invoice->sellerCode : '';

            $arrData['user'] = $tt . $seller;
        }

        return $arrData;
    }

    private function getGoods($appId)
    {
        $goodsArray = [];

        $result = DB::table('tf.goods')
                     ->select('good_mark as mark', 'good_model as model', 'goos_cost as price')
                     ->where('app_id', $appId)
                     ->get();

        foreach ($result as $item) {
            $temoArr['name']    = $item->mark . " " . $item->model;
            $temoArr['count']   = 1;
            $temoArr['amount']  = (float)number_format($item->price, 2, '.', '');

            $goodsArray[] = $temoArr;
        }
        return $goodsArray;
    }

}
