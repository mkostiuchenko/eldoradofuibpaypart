<?php
namespace App\Bank\Create;

use App\Contracts\CreateAppToSendInterface;

class CreateAppToSendBankChoice implements CreateAppToSendInterface {

    public function create($leadId) {

        $arr['StoreId'] = config('cardserviceposConfig.cs_StoreId');
        $arr['OrderId'] = $leadId->ab_lead_id;
        $arr['OrderStatus'] = "Docs accept";
        $arr['Reason'] = "Запрос печатных форм с предварительным согласием клиента";

        return $arr;
    }

}
