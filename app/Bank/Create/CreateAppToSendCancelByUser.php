<?php
namespace App\Bank\Create;

use App\Contracts\CreateAppToSendInterface;

class CreateAppToSendCancelByUser implements CreateAppToSendInterface {

    public function create($leadId) {

        $arr['StoreId'] = config('cardserviceposConfig.cs_StoreId');
        $arr['OrderId'] = $leadId->ab_lead_id;
        $arr['OrderStatus'] = "Client cancel";
        $arr['Reason'] = "Отказ клиента по сделке";

        return $arr;
    }

}
