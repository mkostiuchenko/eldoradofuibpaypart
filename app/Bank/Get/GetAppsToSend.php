<?php
namespace App\Bank\Get;

use App\Contracts\GetAppsToSendInterface;
use App\Helpers\AppTypes;
use Illuminate\Support\Facades\DB;

class GetAppsToSend implements GetAppsToSendInterface {

    public function get() {
        return DB::table('tf.applications as a')
            ->select(
                'a.id as orderId',                                                                       //Ид заявки
                'a.app_mobile_phone as mPhone',                                                                  //Мобильный телефон клиента
                'a.app_client_name',
                'a.app_client_patronymic',
                'a.app_client_surname',
                'n.name_reg_short',
                'n.rccf_sf_store',
                'a.select_month as orderTerm',                                                                   //Срок кредита в месяцах
                'a.app_acc_num',                                                                                 //Номер счета фактуры
                DB::raw('convert(varchar(10), a.app_acc_date, 23) AS app_acc_date'),                      //Дата счета фактуры
                DB::raw('isnull(a.app_first_payment, 0) as down_payment'),                                 //ПВ
                'a.app_type'                                                                                     //Тип заявки
            )
            ->leftJoin('tf.app_banks          as banks', 'banks.app_id',   '=', 'a.id')
            ->leftJoin('tf.m_region_nets      as n',     'n.id',           '=', 'a.id_tt')
            ->leftJoin('dbo.aspnet_Membership as asm',   'asm.UserId',     '=', 'a.idMemberShip_author')
            ->leftJoin('dbo.aspnet_Users      as asu',   'asu.UserId',    '=', 'a.idMemberShip_author')
            ->where([
                ['banks.ab_phase_id', 0],
                ['banks.bank_id', config('bankConfig.bankId')],
                ['banks.ab_error_code', 0]
            ])
            ->whereIn('a.app_type', AppTypes::getAppTypes(config('bankConfig.bankName')))
            ->get();
    }
}
