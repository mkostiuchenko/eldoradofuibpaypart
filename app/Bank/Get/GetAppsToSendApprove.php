<?php
namespace App\Bank\Get;

use App\Contracts\GetAppsToSendInterface;
use App\Helpers\AppTypes;
use Illuminate\Support\Facades\DB;

class GetAppsToSendApprove implements GetAppsToSendInterface
{
    public function get()
    {
        return DB::table('tf.app_banks as ab')
                  ->select('ab.app_id', 'ab.ab_lead_id')
                  ->leftJoin('tf.applications as a', 'a.id', '=', 'ab.app_id')
                  ->where('ab.ab_phase_id', '=', 7)
                  ->where('ab.ab_confirm_sent', '=', 0)
                  ->where('ab.bank_id', config('bankConfig.bankId'))
                  ->where('ab.ab_error_code', 0)
                  ->whereIn('a.app_type', AppTypes::getAppTypes(config('bankConfig.bankName')))
                  ->get();
    }
}
