<?php
namespace App\Bank\Get;

use App\Contracts\GetAppsToSendInterface;
use App\Helpers\AppTypes;
use Illuminate\Support\Facades\DB;

class GetAppsToSendCancelByUser implements GetAppsToSendInterface
{
    public function get()
    {
        return DB::table('tf.app_banks as ab')
                  ->select('ab.app_id', 'ab.ab_lead_id')
                  ->leftJoin('tf.applications as a', 'a.id', '=', 'ab.app_id')
                  ->where('ab.ab_phase_id', '=', 9)
                  ->where('ab.ab_cancel_sent', '=', 0)
                  ->where('ab.ab_error_code', '=', 0)
                  ->where('ab.bank_id', config('bankConfig.bankId'))
                  ->whereIn('a.app_type', AppTypes::getAppTypes(config('bankConfig.bankName')))
                  ->get();
    }
}
