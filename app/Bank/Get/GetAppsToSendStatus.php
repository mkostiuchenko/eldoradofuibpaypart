<?php
namespace App\Bank\Get;

use App\Contracts\GetAppsToSendInterface;
use Illuminate\Support\Facades\DB;

class GetAppsToSendStatus  implements GetAppsToSendInterface
{
    public function get() {
        $data = DB::select('SELECT ab.app_id
                                   , ab.ab_lead_id
                              FROM tf.app_banks ab
                          (NOLOCK)
				        INNER JOIN tf.applications a ON a.id = ab.app_id
                             WHERE ab.ab_phase_id IN (1)
                               AND ab.ab_error_code = 0
                               AND ab.bank_id = ' . config('bankConfig.bankId') . '
                               AND a.app_type in (16)');

        return $data;
    }
}
