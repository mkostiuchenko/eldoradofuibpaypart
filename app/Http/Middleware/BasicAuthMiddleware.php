<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('Authorization')) {
            $result = false;
        } else {
            $tokenConfig = "Basic " . base64_encode(config('bankConfig.basicauth_login') . ':' . config('bankConfig.basicauth_password'));
            $result = false;
            if ($tokenConfig == $request->header('Authorization')) {
                $result = true;
            }
        }

        if ($result) {
            return $next($request);
        } else {
            return response()->json( [
                'statusCode' => 1,
                'statusText' => 'Не корректная авторизация',
            ], 404 );
        }
    }
}
