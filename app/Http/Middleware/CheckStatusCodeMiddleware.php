<?php

namespace App\Http\Middleware;

use Closure;

class CheckStatusCodeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $itog = [];
        $result = true;
        if (!isset($request->state) || empty($request->state)) {
            $itog['statusCode'] = 1;
            $itog['statusText'] = 'Отсутствует или пустой параметр state!';
            $result = false;
        }

        if ($result) {
            return $next($request);
        } else {
            return response()->json($itog, 404);
        }
    }
}
