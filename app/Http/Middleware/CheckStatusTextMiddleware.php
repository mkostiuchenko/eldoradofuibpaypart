<?php

namespace App\Http\Middleware;

use Closure;

class CheckStatusTextMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $itog = [];
        $result = true;
        if (!isset($request->statusText) || empty($request->statusText)) {
            $itog['statusCode'] = "ERROR_STATUSTEXT";
            $itog['statusText'] = 'Отсутствует или пустой параметр statusText!';
            $result = false;
        }

        if ($result) {
            return $next($request);
        } else {
            return response()->json($itog, 404);
        }
    }
}
