<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Logger;
use Closure;

class CheckAppId {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $logger = new Logger($request->prefix, config('logFileName.noAppId'));
        if (!isset($request->app_id) || empty($request->app_id)) {

            $result['res_number'] = 1;
            $result['res_txt']    = 'Не передан номер заявки';

            $logger->makeLog(" - Не передан номер заявки - " . json_encode($request->all()));
            //Logger::makeLog($request->prefix, "errors" . DIRECTORY_SEPARATOR . "noAppId.log", " - Не передан номер заявки - " . json_encode($request->all()));

            return response()->json($result);
        }

        return $next($request);
    }
}
