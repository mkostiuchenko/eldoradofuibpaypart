<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class mappingAuth {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        $currentTime = time();
        $result = DB::table('tf.token_oauth2')
            ->where('token', $request->header('token'))
            ->select('bank_id', 'lifetime_token', 'date_token', 'login', 'password', 'token')
            ->first();

        if (!$result) {
            return response()->json( [
                'res_number' => 1,
                'res_txt'    => 'Authorization failed',
            ], 404 );
        }

        $carbon = new Carbon();
        $date_token = strtotime($carbon->toDateTimeString($result->date_token));
        $lifetime_token = strtotime($carbon->toDateTimeString($result->lifetime_token));

        $lifetime_token = $date_token + $lifetime_token;

        if(trim($result->token) === trim($request->header('token')) && ($currentTime < $lifetime_token)) {
            $request['bankId'] = $result->bank_id;
            return $next($request);
        }
    }
}
