<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckCapIdMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $itog = [];
        if (!isset($request->cap_id) || empty($request->cap_id)) {
            $itog['statusCode'] = 1;
            $itog['statusText'] = 'Параметр cap_id отсутствует или пустой!';
            $result = false;
        } else {
            $appId = DB::table('tf.app_banks')
                ->where([
                    ['ab_lead_id', (string)$request->cap_id],
                    ['bank_id', (int)config('bankConfig.bankId')]
                ])
                ->select('app_id')
                ->first();

            $result = true;

            if (empty($appId)){
                $itog['statusCode'] = 1;
                $itog['statusText'] = 'По указанному значению параметра leadId=' . $request->cap_id . ' нет заявки в системе Таргет Финанс!';
                $result = false;
            }
        }

        if ($result) {
            return $next($request);
        } else {
            return response()->json($itog, 404);
        }
    }
}
