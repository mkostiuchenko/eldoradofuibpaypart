<?php
namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class Errors {
    public static function appStatusUpdateInError($appId, $bankId, $message) {
        DB::table('tf.app_banks')
            ->where([
                ['bank_id', $bankId],
                ['app_id', $appId]
            ])
            ->update([
                'ab_error_code' => 1,
                'ab_error_text' => (string)$message,
                'ab_answer_comment' => (string)$message,
                'ab_answer' => 'ERR',
            ]);
    }
}
