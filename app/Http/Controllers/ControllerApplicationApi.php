<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ControllerApplicationApi extends Controller {

    public function setSignatureData(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetSignature"), $request);

        return response()->json($status);
    }

    public function refundData(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetRefundData"), $request);

        return response()->json($status);
    }

    public function statusRefund(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetRefundStatus"), $request);

        return response()->json($status);
    }

    public function recieveCallback(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->recieve(App::make("App\Bank\Receive\RecieveCallback"), $request);

        return response()->json($status);
    }

    public function test(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\TEST"), $request);

        return response()->json($status);
    }

    public function setPriliminaryApproved(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetPriliminaryApproved"), $request);

        return response()->json($status);
    }

    public function setNeedFullApp(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetNeedFullApp"), $request);

        return response()->json($status);
    }

    public function setApproved(Request $request) {
        $appApi = new ApplicationApi();
        $status = $appApi->set(App::make("App\Bank\Set\SetApproved"), $request);

        return response()->json($status);
    }

    public function setCancel(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetCancel"), $request);

        return response()->json($status);
    }

    public function setCancelByClient(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->set(App::make("App\Bank\Set\SetCancelByClient"), $request);

        return response()->json($status);
    }

    public function recieveDocs(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->recieve(App::make("App\Bank\Receive\RecieveDocs"), $request);

        return response()->json($status);
    }

    public function recieveRework(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->recieve(App::make("App\Bank\Receive\RecieveRework"), $request);

        return response()->json($status);
    }

    public function receiveState(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->recieve(App::make("App\Bank\Receive\ReceiveState"), $request);

        return response()->json($status);
    }

    public function sendApprove(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->send(App::make("App\BankSend\SendApprove"), $request);

        return response()->json($status);
    }

    public function sendСard(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->send(App::make("App\Bank\Send\SendCard"), $request);

        return response()->json($status);
    }
    public function sendCardCancel(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->send(App::make("App\Bank\SendCardCancel"), $request);

        return response()->json($status);
    }

    public function sendDocs(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->send(App::make("App\Bank\Send\SendDocs"), $request);

        return response()->json($status);
    }

    public function sendSms(Request $request) {

        $appApi = new ApplicationApi();

        $status = $appApi->send(App::make("App\Bank\Send\SendSms"), $request);

        return response()->json($status);
    }

    public function sendSmsRequest(Request $request) {
        $appApi = new ApplicationApi();

        $status = $appApi->send(App::make("App\Bank\Send\SendSmsRequest"), $request);

        return response()->json($status);
    }
}
