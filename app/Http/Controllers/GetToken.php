<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetToken {
    public function get(Request $request) {

        $check = DB::table('tf.token_oauth2')
            ->where([
                ['login', $request->login],
                ['password', $request->password]
            ])
            ->exists();

        if (!$check) {
            return response()->json( "Authorization failed" );
        }

        $token = password_hash(crypt($request->login, rand(9999, 999999)).$request->login.crypt($request->login, rand(9999, 999999)).rand(9999, 999999), PASSWORD_DEFAULT);

        DB::table('tf.token_oauth2')
            ->where('login', $request->login)
            ->update([
                'token' => $token,
                'date_token' => date('Y.m.d H:i:s', time()),
                'lifetime_token' => config($request->login . 'Config.tokenLife')
            ]);

        return response()->json( [
            'access_token' => $token,
            'expires_in'   => config($request->login . 'Config.tokenLife')
        ] );
    }
}
