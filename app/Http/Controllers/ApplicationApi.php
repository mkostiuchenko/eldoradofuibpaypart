<?php

namespace App\Http\Controllers;

use App\Contracts\RecieveInterface;
use App\Contracts\SendRequestInterface;
use App\Contracts\SetInterface;

class ApplicationApi extends Controller {

    public function set(SetInterface $bank, $request) {
        return $bank->set($request);
    }

    public function recieve(RecieveInterface $bank, $request) {
        return $bank->recieve($request);
    }

    public function send(SendRequestInterface $bank, $app) {
        return $bank->send($app);
    }
}
