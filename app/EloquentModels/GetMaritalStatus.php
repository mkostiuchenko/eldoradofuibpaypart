<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetMaritalStatus extends Model {
    protected $table = 'tf.s_marital_status';
    public $timestamps = false;
}
