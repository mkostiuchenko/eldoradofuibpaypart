<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetWorkOwnership extends Model
{
    protected $table = 'tf.sm_wrk_ownership';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
