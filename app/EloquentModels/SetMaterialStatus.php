<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetMaterialStatus extends Model {
    protected $table = 'tf.sm_marital_status';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
