<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetWorkCategory extends Model
{
    protected $table = 'tf.s_wrk_category';
    public $timestamps = false;
}
