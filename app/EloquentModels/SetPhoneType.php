<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetPhoneType extends Model
{
    protected $table = 'tf.sm_phone_types';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
