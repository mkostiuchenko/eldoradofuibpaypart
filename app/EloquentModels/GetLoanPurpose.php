<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetLoanPurpose extends Model
{
    protected $table = 'tf.loan_purpose';
    public $timestamps = false;
}
