<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetWorkOwnership extends Model
{
    protected $table = 'tf.s_wrk_ownership';
    public $timestamps = false;
}
