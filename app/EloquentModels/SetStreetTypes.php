<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetStreetTypes extends Model
{
    protected $table = 'tf.sm_street_types';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
