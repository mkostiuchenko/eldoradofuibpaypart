<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetWorkType extends Model
{
    protected $table = 'tf.s_wrk_type';
    public $timestamps = false;
}
