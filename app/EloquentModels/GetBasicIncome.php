<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetBasicIncome extends Model
{
    protected $table = 'tf.s_basic_income';
    public $timestamps = false;
}
