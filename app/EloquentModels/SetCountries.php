<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetCountries extends Model
{
    protected $table = 'tf.sm_countries';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
