<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetCities extends Model
{
    protected $table = 'tf.s_cities';
    public $timestamps = false;
}
