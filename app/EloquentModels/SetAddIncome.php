<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetAddIncome extends Model {
    protected $table = 'tf.sm_add_income';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
