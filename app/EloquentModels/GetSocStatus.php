<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetSocStatus extends Model
{
    protected $table = 'tf.s_soc_status';
    public $timestamps = false;
}
