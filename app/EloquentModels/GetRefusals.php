<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetRefusals extends Model
{
    protected $table = 'tf.s_refusals';
    public $timestamps = false;
}
