<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetEducation extends Model
{
    protected $table = 'tf.sm_educations';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
