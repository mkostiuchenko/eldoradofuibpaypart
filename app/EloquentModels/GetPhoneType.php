<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetPhoneType extends Model
{
    protected $table = 'tf.s_phone_types';
    public $timestamps = false;
}
