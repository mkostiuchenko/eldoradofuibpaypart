<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetWorkActivity extends Model
{
    protected $table = 'tf.s_wrk_activity';
    public $timestamps = false;
}
