<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetLiveOwnership extends Model
{
    protected $table = 'tf.sm_live_ownership';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
