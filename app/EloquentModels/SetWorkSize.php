<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetWorkSize extends Model
{
    protected $table = 'tf.sm_wrk_size';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
