<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetAreas extends Model
{
    protected $table = 'tf.s_areas';
    public $timestamps = false;
}
