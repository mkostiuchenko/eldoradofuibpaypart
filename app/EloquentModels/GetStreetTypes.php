<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetStreetTypes extends Model
{
    protected $table = 'tf.s_street_types';
    public $timestamps = false;
}
