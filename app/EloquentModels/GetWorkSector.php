<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetWorkSector extends Model
{
    protected $table = 'tf.s_wrk_sector';
    public $timestamps = false;
}
