<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetRelations extends Model
{
    protected $table = 'tf.sm_cp_relations';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
