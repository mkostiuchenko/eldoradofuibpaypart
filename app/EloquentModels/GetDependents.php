<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetDependents extends Model
{
    protected $table = 'tf.s_dependents';
    public $timestamps = false;
}
