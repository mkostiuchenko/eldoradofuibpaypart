<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetJobTitle extends Model
{
    protected $table = 'tf.s_wrk_job_title';
    public $timestamps = false;
}
