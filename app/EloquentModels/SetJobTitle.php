<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetJobTitle extends Model
{
    protected $table = 'tf.sm_wrk_job_title';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
