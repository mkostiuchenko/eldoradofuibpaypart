<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetGoodsGroup extends Model {
    protected $table = 'tf.s_goods_groups';
    public $timestamps = false;
}
