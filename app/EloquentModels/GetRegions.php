<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetRegions extends Model
{
    protected $table = 'tf.s_regions';
    public $timestamps = false;
}
