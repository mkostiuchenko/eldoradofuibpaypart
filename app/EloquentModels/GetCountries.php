<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetCountries extends Model
{
    protected $table = 'tf.s_countries';
    public $timestamps = false;
}
