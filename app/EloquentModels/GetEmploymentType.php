<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetEmploymentType extends Model
{
    protected $table = 'tf.s_employment_type';
    public $timestamps = false;
}
