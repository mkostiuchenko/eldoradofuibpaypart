<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetCreditType extends Model
{
    protected $table = 'tf.s_cred_type';
    public $timestamps = false;
}
