<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetGoods extends Model
{
    protected $table = 'tf.s_goods';
    public $timestamps = false;
}
