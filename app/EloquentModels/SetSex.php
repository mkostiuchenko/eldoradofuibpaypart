<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetSex extends Model
{
    protected $table = 'tf.sm_sex';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
