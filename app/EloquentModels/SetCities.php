<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetCities extends Model
{
    protected $table = 'tf.sm_cities';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
