<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetAddIncome extends Model {
    protected $table = 'tf.s_add_income';
    public $timestamps = false;
}
