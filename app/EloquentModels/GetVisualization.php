<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetVisualization extends Model
{
    protected $table = 'tf.s_visualization';
    public $timestamps = false;
}
