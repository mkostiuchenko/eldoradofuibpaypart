<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetCityType extends Model
{
    protected $table = 'tf.s_city_types';
    public $timestamps = false;
}
