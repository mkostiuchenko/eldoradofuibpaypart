<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetRelations extends Model
{
    protected $table = 'tf.s_cp_relations';
    public $timestamps = false;
}
