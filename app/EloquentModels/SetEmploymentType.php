<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetEmploymentType extends Model
{
    protected $table = 'tf.sm_employment_type';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
