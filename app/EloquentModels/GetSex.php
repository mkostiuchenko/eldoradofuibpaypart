<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetSex extends Model
{
    protected $table = 'tf.s_sex';
    public $timestamps = false;
}
