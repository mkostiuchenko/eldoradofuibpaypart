<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetRealpropertyType extends Model
{
    protected $table = 'tf.s_rp_types';
    public $timestamps = false;
}
