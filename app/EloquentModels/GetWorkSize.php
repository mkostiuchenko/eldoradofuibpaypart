<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetWorkSize extends Model
{
    protected $table = 'tf.s_wrk_size';
    public $timestamps = false;
}
