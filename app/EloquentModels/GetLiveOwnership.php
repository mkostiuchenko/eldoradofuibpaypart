<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetLiveOwnership extends Model
{
    protected $table = 'tf.s_liv_ownership';
    public $timestamps = false;
}
