<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetOwnershipKind extends Model
{
    protected $table = 'tf.s_ownreship_kind';
    public $timestamps = false;
}
