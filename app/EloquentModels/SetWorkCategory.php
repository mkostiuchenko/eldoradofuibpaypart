<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetWorkCategory extends Model
{
    protected $table = 'tf.sm_wrk_category';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
