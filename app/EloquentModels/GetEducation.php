<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetEducation extends Model
{
    protected $table = 'tf.s_educations';
    public $timestamps = false;
}
