<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class GetGoodsSubGroup extends Model
{
    protected $table = 'tf.s_goods_subgroups';
    public $timestamps = false;
}
