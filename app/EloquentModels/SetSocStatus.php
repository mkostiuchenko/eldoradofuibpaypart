<?php

namespace App\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class SetSocStatus extends Model
{
    protected $table = 'tf.sm_soc_status';
    public $timestamps = false;

    protected $fillable = [
        'bank_id', 'reference_id', 'value_id', 'value_description'
    ];
}
