<?php
namespace App\Console\Commands;

use App\Bank\Get\GetAppsToSendApprove;
use App\Bank\Send\SendApprove;
use Illuminate\Console\Command;

class PayPartSendApprove extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fuibpaypart:sendapprove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $infostatus = new GetAppsToSendApprove();
        $appsArray = $infostatus->get();

        if (empty($appsArray)) {
            return;
        }

        $sendApprove = new SendApprove();
        foreach ($appsArray as $item) {
            $sendApprove->send($item);
        }
    }
}
