<?php

namespace App\Console\Commands;

use http\Client\Request;
use Illuminate\Console\Command;
use App\Bank\Get\GetAppsToSend;
use App\Bank\Send\SendApp;

class PayPartSendApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fuibpaypart:sendapp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $infostatus = new GetAppsToSend();
        $appsArray = $infostatus->get();

        if (empty($appsArray)) {
            return;
        }

        $sendApp = new SendApp();
        foreach ($appsArray as $item) {
            $sendApp->send($item);
        }
    }
}
