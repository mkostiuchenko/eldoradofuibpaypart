<?php
namespace App\Console\Commands;

use App\Bank\Send\SendGaranteeLetter;
use App\Bank\Get\GetAppsToSendGuaranteeLetter;
use Illuminate\Console\Command;

class PayPartGaranteeLetter extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fuibpaypartguaranteeletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $infostatus = new GetAppsToSendGuaranteeLetter();
        $appsArray = $infostatus->get();

        if (empty($appsArray)) {
            return;
        }

        $sendGaranteeLetter = new SendGaranteeLetter();
        foreach ($appsArray as $item) {
            $sendGaranteeLetter->send($item);
        }
    }
}
