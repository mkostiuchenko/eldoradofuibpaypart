<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Bank\Send\SendStatus;
use App\Bank\Get\GetAppsToSendStatus;

class PayPartGetLoanStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fuibpaypart:sendstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $infostatus = new GetAppsToSendStatus();
        $appsArray = $infostatus->get();

        if (empty($appsArray)) {
            return;
        }

        $sendStatus = new SendStatus();
        foreach ($appsArray as $item) {
            $sendStatus->send($item);
        }
    }
}
