<?php

namespace App\Console\Commands;

use App\Bank\Get\GetAppsToSendCancelByUser;
use App\Bank\Send\SendCancelByUser;
use Illuminate\Console\Command;

class PayPartSendCancelClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fuibpaypart:sendcancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $infostatus = new GetAppsToSendCancelByUser();
        $appsArray = $infostatus->get();

        if (empty($appsArray)) {
            return;
        }

        $sendApp = new SendCancelByUser();
        foreach ($appsArray as $item) {
            $sendApp->send($item);
        }
    }
}
