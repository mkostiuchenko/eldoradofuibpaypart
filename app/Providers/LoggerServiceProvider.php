<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Logger;
//use Symfony\Component\HttpKernel\Tests\Logger;

class LoggerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Logger', function (){
            return new Logger();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
