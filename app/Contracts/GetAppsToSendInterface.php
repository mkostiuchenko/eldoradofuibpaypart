<?php
namespace App\Contracts;

interface GetAppsToSendInterface {
    public function get();
}
