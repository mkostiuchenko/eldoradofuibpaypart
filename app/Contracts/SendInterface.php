<?php
namespace App\Contracts;

interface SendInterface {
    public function send($app, $method);
}
