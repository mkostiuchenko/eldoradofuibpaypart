<?php
namespace App\Contracts;

interface CreateAppToSendAgreementInterface {
    public function create($leadId, $proposalId);
}
