<?php
namespace App\Contracts;

interface CheckAppIfCanGetStateInterface {
    public function check($appId, $appPhase, $appAnswer);
}
