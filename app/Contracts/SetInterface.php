<?php
namespace App\Contracts;

interface SetInterface {
    public function set($request);
}
