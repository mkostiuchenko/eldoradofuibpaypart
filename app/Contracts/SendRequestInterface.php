<?php
namespace App\Contracts;

interface SendRequestInterface {
    public function send($app);
}
