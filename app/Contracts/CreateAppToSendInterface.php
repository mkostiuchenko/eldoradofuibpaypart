<?php
namespace App\Contracts;

interface CreateAppToSendInterface {
    public function create($app);
}
