<?php
namespace App;

use App\Contracts\SetInterface;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\AppTypes;

class TEST implements SetInterface {

    public function set($request) {
      return DB::table('tf.applications as a')
            ->select(
                'a.id',
                'a.app_type',
                'a.guid_id',
                'n.id_nets',
                'a.app_client_surname',
                'a.app_client_name',
                'a.app_client_patronymic',
                'a.app_client_email',
                'a.app_client_inn',
                'a.app_inn_date',
                'a.app_inn_date',
                'a.app_client_visual',
                'a.app_marital_status',
                'sms.value_description as maritalStatus',
                'a.app_change_year',
                'a.app_m_name',
                'a.app_m_surname',
                'a.app_m_patronymic',
                'a.app_m_soc_status',
                'a.app_citizenship',
                'a.app_birth_date',
                'a.app_education',
                'se.value_description as education',
                'a.app_pasp_s',
                'a.app_pasp_n',
                'a.app_pasp_i',
                'a.app_pasp_d',
                'a.app_dependents',
                'a.app_childrens',
                'a.app_password',
                'a.app_acc_num',
                'a.app_acc_date',
                'a.app_birth_country',
                'a.app_birth_place',
                'a.app_first_payment',
                'a.app_need_insurance',
                'a.app_cred_term',
                'a.app_reg_phone',
                'a.app_liv_phone',
                'a.app_begin_live',
                'a.app_reg_date',
                'a.app_mobile_phone',
                'a.app_home_phone',
                'a.app_wrk_bphone',
                'a.app_work_phone',
                'a.app_user_id',
                'a.app_bank_id',
                'a.app_product_id',
                'a.app_created',
                'a.app_state',
                'a.app_cp1_surname',
                'a.app_cp1_name',
                'a.app_cp1_patronymic',
                'a.app_cp1_ph1',
                'cp.value_description as relationType',
                'a.app_basic_income',
                'a.app_add_income',
                'a.app_liv_ownership',
                'slo.value_description as liveOwnership',
                'a.app_basic_summa',
                'a.app_add_summa',
                'a.app_add_comment',
                'a.app_sum_outcame as averageCosts',
                'a.app_basic_confirmation',
                'a.app_last_credits5',
                'a.app_open_credits',
                'a.app_wrk_okpo',
                'a.app_wrk_category',
                'a.app_wrk_owner',
                'a.app_wrk_sector',
                'a.app_wrk_activity',
                'a.app_wrk_job_title',
                'a.app_wrk_position',
                'a.app_wrk_name',
                'a.app_wrk_age as experience',
                'a.app_liv_ownership',
                'a.app_wrk_start_date as experienceLast',
                'sx.value_description as clientSex',
                'ss.value_description as proffStatus',
                'swjt.value_description as occupation',
                'sws.value_description as jobSegment',
                'swc.value_description as wrkCategory',
                'sv.value_description as clientVisual',
                'lc.idSovFak',
                'a.month_SovFakt',
                'a.id_tt',
                'n.SovFak_tt_id',
                'n.SovFak_net_id',
                'am.patronymic',
                'am.name',
                'am.surname',
                'am.Email as managerEMail',
                'am.phone_mob as managerPhone',
                'a.app_m_surname',
                'a.app_m_name',
                'a.app_m_patronymic',
                'a.numberIDCard',
                'a.whoIssueIDCard',
                'a.validUntilIDCard',
                'a.typeDoc',
                'a.NotIDCard'
            )
            ->leftJoin('tf.app_banks          as banks', function ($join){
                $join->on('banks.app_id', '=', 'a.id')->where('banks.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.m_region_nets      as n',     'n.id',         '=', 'a.id_tt')
            ->leftJoin('dbo.aspnet_Membership as am',    'am.UserId',    '=', 'a.IdMemberShip_author')
            ->leftJoin('tf.applications_bsp as absp', function ($join){
                $join->on('absp.app_id', '=', 'a.id')->where('absp.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.loan_credit        as lc', 'lc.id',     '=', 'absp.product_id')
            ->leftJoin('tf.sm_marital_status as sms', function ($join){
                $join->on('sms.reference_id', '=', 'a.app_marital_status')->where('sms.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_educations as se', function ($join){
                $join->on('se.reference_id', '=', 'a.app_education')->where('se.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_cp_relations as cp', function ($join){
                $join->on('cp.reference_id', '=', 'a.app_cp1_relation')->where('cp.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_live_ownership as slo', function ($join){
                $join->on('slo.reference_id', '=', 'a.app_liv_ownership')->where('slo.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_sex as sx', function ($join){
                $join->on('sx.reference_id', '=', 'a.app_client_sex')->where('sx.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_soc_status as ss', function ($join){
                $join->on('ss.reference_id', '=', 'a.app_soc_status')->where('ss.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_wrk_job_title as swjt', function ($join){
                $join->on('swjt.reference_id', '=', 'a.app_wrk_job_title')->where('swjt.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_wrk_sector as sws', function ($join){
                $join->on('sws.reference_id', '=', 'a.app_wrk_sector')->where('sws.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_wrk_category as swc', function ($join){
                $join->on('swc.reference_id', '=', 'a.app_wrk_category')->where('swc.bank_id', '=', config('banksId.sf'));
            })
            ->leftJoin('tf.sm_visualization as sv', function ($join){
                $join->on('sv.reference_id', '=', 'a.app_client_visual')->where('sv.bank_id', '=', config('banksId.sf'));
            })
            ->where([
                ['banks.ab_phase_id', 2],
                ['banks.app_id', 1035348],
                ['banks.bank_id', config('banksId.sf')],
                ['banks.ab_error_code', 0]
            ])
            ->whereIn('a.app_type', AppTypes::getAppTypes(config('banksName.sf_pos')))
            ->get();


    }
}
