<!DOCTYPE html>
<html>
<head>
    <title>Гарантийный талон</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        *{ font-family: DejaVu Sans !important;}
    </style>
</head>
<body>
<div style="width: 100%; max-width: 960px; margin: auto">
    <table width="100%" cellpadding="0" cellspacing="0" border="1">
        <tbody>
        <tr>
            <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Идентификатор Листа-гарантии</td>
            <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['id_garantee_letter'] }}</td>
        </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">ID заявки в системе банка</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['request_id'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Дата и время отправки данного письма банком</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['answer_datetime'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Организация-отправитель</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['from_organization'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">ОКПО организации-отправителя</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['organization_id'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">ФИО клиента</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['last_name'] . " " . $answer['first_name'] . " " .  $answer['middle_name'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Инн клиента</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['inn'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Серия документа</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['series'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Номер документа</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['number'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Орган выдачи документа</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['issued'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Дата выдачи документа</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['date_of_issue'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Номер счета-фактуры</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['invoice_number'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Дата счета-фактуры</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['invoice_date'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Сумма счета-фактуры</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['invoice_amount'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">ЕГРПОУ партнера</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['dest_id'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">/Название партнера</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['dest_name'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">МФО банка, в котором открыт счет для зачисления средств</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['dest_bank_mfo'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Наименование банка, в котором открыт счет для зачисления средств</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['dest_bank_name'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Номер кредитного договора между банком и клиентом</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['contract_number'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Информация о магазине, где состоялась продажа в кредит (Адрес магазина)</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['store_address'] }}</td>
         </tr>
         <tr>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">Информация о магазине, где состоялась продажа в кредит (Код точки продаж)</td>
             <td style="text-align: left; padding: 5px 10px; font-size: 10px;">{{ $answer['store_code'] }}</td>
         </tr>
        </tbody>
    </table>
</div>
</body>
</html>
