<?php
use Illuminate\Support\Facades\DB;

Route::get('/', function () {
    return view('welcome');
});

Route::get("/test", function(){
    return view('test');
})->name('test');
